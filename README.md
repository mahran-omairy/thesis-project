# KGBrowser configuration

This repo contains the tool for creating the Knowledge Graph configuration

[Demo](https://kgbrowser-config.me/)

[Backend](https://api.kgbrowser-config.me/docs/)

To run the project locally you need docker to be installed on you device

## Project run locally

```
docker-compose up -d
```

## Front-end app

Navigate to http://localhost/list

## Back-end app

Navigate to http://localhost:5000/docs for swagger UI
