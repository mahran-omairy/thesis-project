import Vue from "vue";
import App from "./App.vue";
import VueFilterDateFormat from "@vuejs-community/vue-filter-date-format";
import VueCytoscape from "vue-cytoscape";
import VueRouter from "vue-router";
import vuetify from "./plugins/vuetify";
import store from "./store";
import List from "./pages/list";
import Manange from "./pages/manage";
import "./styles/general.scss";

Vue.use(VueRouter);
Vue.config.productionTip = false;
Vue.use(VueCytoscape);
Vue.use(VueFilterDateFormat, {
  timezone: 120,
});

const routes = [
  { path: "/list", component: List },
  { path: "/manage/:id?", component: Manange },
  { path: "*", redirect: "/list" },
];
const router = new VueRouter({
  mode: "history",
  routes,
});

new Vue({
  vuetify,
  store,
  router,
  render: (h) => h(App),
}).$mount("#app");
