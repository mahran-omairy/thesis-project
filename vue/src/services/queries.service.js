import axios from "axios";
import { baseUrl } from "./api";
export default class Queries {
  /**
   *
   * data sample
   * {
   * sparql: string
   * resource: string
   * dataset: {
   *  sparqlEndpoint: string
   *  accept: string
   *  }
   * }
   */

  /**
   * send a api call to get the results of an expansion query
   * @param {Object} data query data
   * @returns an axios response
   */
  static executeExpand(data) {
    return axios.post(`${baseUrl}/queries/expand`, data);
  }

  /**
   * send a api call to get the results of a preview query
   * @param {Object} data query data
   * @returns an axios response
   */
  static executePreview(data) {
    return axios.post(`${baseUrl}/queries/preview`, data);
  }

  /**
   * send a api call to get the results of an details query
   * @param {Object} data query data
   * @returns an axios response
   */
  static executeDetails(data) {
    return axios.post(`${baseUrl}/queries/details`, data);
  }

  /**
   * send an api call to collect information about a configuration representation
   * @param {String} uri the configuration iri
   * @returns an axios response
   */
  static fetchQuery(uri) {
    return axios.get(`${baseUrl}/queries/details`, {
      params: { uri: uri },
    });
  }

  /**
   * send an api call to fetch the meta configuration data
   * @returns an axios response
   */
  static fetchMeta() {
    return axios.get(`${baseUrl}/queries/meta-configuration`, {
      params: {
        uri:
          "https://linked.opendata.cz/resource/knowledge-graph-browser/meta-configuration/all-configurations",
      },
    });
  }

  /**
   * send an api to fetch all componenets of the selected meta configurations
   * @param {Array} metaList a list of configurations iris
   * @returns an axios response
   */
  static fetchSuggestions(metaList) {
    return axios.post(`${baseUrl}/queries/suggestions`, {
      metaList: metaList.map((el) => el.id),
    });
  }
}
