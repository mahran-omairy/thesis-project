import axios from "axios";
import { baseUrl } from "./api";
export default class Files {
  /**
   * make an api call to list the saved configuration files
   * @returns an axios response
   */
  static list() {
    return axios.get(`${baseUrl}/files`);
  }

  /**
   * send an api call to save a configuration file on the server
   * @param {Object} file
   *  @returns an axios response
   */
  static create(file) {
    return axios.post(`${baseUrl}/files`, file);
  }

  /**
   * send an api call to get a configuration file by its id
   * @param {Number} id the id of the file
   * @returns an axios response
   */
  static get(id) {
    return axios.get(`${baseUrl}/files/${id}`);
  }

  /**
   * send an api call to delete a configuration file by its id
   * @param {Number} id the id of the file
   * @returns an axios response
   */
  static delete(id) {
    return axios.delete(`${baseUrl}/files/${id}`);
  }

  /**
   * send an api call to update a configuration file by its id
   * @param {Number} id the id of the file
   * @param {Object} file file data
   * @returns an axios response
   */
  static update(id, file) {
    return axios.put(`${baseUrl}/files/${id}`, file);
  }
}
