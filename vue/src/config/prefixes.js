const PREFIXES = [
  {
    prefix: "browser",
    uri: "https://linked.opendata.cz/ontology/knowledge-graph-browser/",
  },
  {
    prefix: "dct",
    uri: "http://purl.org/dc/terms/",
  },
  {
    prefix: "owl",
    uri: "http://www.w3.org/2002/07/owl#",
  },
  {
    prefix: "rdfs",
    uri: "http://www.w3.org/2000/01/rdf-schema#",
  },
  {
    prefix: "void",
    uri: "http://rdfs.org/ns/void#",
  },
];

export default PREFIXES;
