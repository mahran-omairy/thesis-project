import { NODE_STYLES, EDGE_STYLES } from "./styles";
/**
 * check if string is a link
 * @param {string} link
 * @returns boolean
 */

export const isLink = (link) => {
  const expression = /https?:\/\/[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&//=]*)?/gi;
  const regex = new RegExp(expression);

  if (link) {
    const matches = link.match(regex);
    return matches ? matches.length > 0 : false;
  }
  return true;
};

/**
 * extract a link from givin string
 * @param {String} link
 * @returns string
 */
export const extractLink = (link) => {
  const expression = /https?:\/\/[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&//=]*)?/gi;
  const regex = new RegExp(expression);
  const matches = link.match(regex);
  return matches.length ? matches[0] : "";
};

/**
 * parse a dataset
 * @param {Object} configs parsed configuration file
 * @param {Array} matches uris of matched data sets
 * @returns array of datasets accessped by the system
 */
const reformDataSet = (configs, matches) => {
  const datasets = [];
  if (matches.length) {
    const matchedDs = configs.datasets.filter((el) => matches.includes(el.uri));
    if (matchedDs.length) {
      datasets.push(
        ...matchedDs.map((el) => ({
          base: { property: "void:Dataset", uri: `<${el.uri}>` },
          id: `<${el.uri}>`,
          addedFrom: [
            ...configs.previewQueries.filter((it) =>
              it.hasDataset.includes(el.uri)
            ),
            ...configs.expansionQueries.filter((it) =>
              it.hasDataset.includes(el.uri)
            ),
            ...configs.detailQueries.filter((it) =>
              it.hasDataset.includes(el.uri)
            ),
            ...configs.viewsets.filter((it) => it.hasDataset.includes(el.uri)),
          ],
          readOnly: true,
          items: [
            { property: "dct:title", value: el.title },
            {
              property: "browser:accept",
              value: el.accept ? el.accept.map((el) => `"${el}"`) : [],
            },
            {
              property: "void:sparqlEndpoint",
              value: el["void#sparqlEndpoint"]
                ? el["void#sparqlEndpoint"].map((el) => `"${el}"`)
                : [],
            },
          ],
        }))
      );
    }
  }
  return datasets;
};

/**
 * parse a query
 * @param {Object} queryData
 * @param {String} property
 * @param {String} parent
 * @returns parsed query object
 */
const reformQuery = (queryData, property, parent) => {
  return {
    base: {
      property: property,
      uri: `<${queryData.uri}>`,
    },
    id: `<${queryData.uri}>`,
    addedFrom: [parent],
    readOnly: true,
    items: [
      {
        property: "dct:title",
        value: queryData.title
          ? queryData.title.map((el) => (el.includes("@") ? el : `"${el}"`))
          : [],
      },
      {
        property: "browser:query",
        value: queryData.query
          ? queryData.query.map((it) => `"""${it}"""`)
          : [],
      },
      {
        property: "browser:hasDataset",
        value: queryData.hasDatase
          ? queryData.hasDataset.map((it) => `<${it}>`)
          : [],
      },
    ],
  };
};
/**
 * parase a view with its queries and dataset
 * @param {Object} configs
 * @param {String} matched
 * @param {String} parent
 * @returns an object the contains the view and its queries
 */
const reformView = (configs, matched, parent) => {
  let view = {};
  let expansion = {};
  let preview = {};
  let detail = {};
  const datasets = [];

  const matchedView = configs.views.find((el) => el.uri === matched);
  if (matchedView) {
    // parse the view
    view = {
      base: { property: "browser:View", uri: `<${matchedView.uri}>` },
      id: `<${matchedView.uri}>`,
      addedFrom: [parent],
      readOnly: true,
      items: [
        {
          property: "dct:title",
          value: matchedView.title
            ? matchedView.title.map((el) => (el.includes("@") ? el : `"${el}"`))
            : [],
        },
        {
          property: "dct:description",
          value: matchedView.description
            ? matchedView.description.map((el) =>
                el.includes("@") ? el : `"${el}"`
              )
            : [],
        },
        {
          property: "browser:hasExpansion",
          value: matchedView.hasExpansion
            ? matchedView.hasExpansion.map((it) => `<${it}>`)
            : [],
        },
        {
          property: "browser:hasPreview",
          value: matchedView.hasPreview
            ? matchedView.hasPreview.map((it) => `<${it}>`)
            : [],
        },
        {
          property: "browser:hasDetail",
          value: matchedView.hasDetail
            ? matchedView.hasDetail.map((it) => `<${it}>`)
            : [],
        },
      ],
    };

    /****** parse queries *******/
    const expansionQuery = configs.expansionQueries.find((el) =>
      matchedView.hasExpansion.includes(el.uri)
    );
    const previewQuery = configs.previewQueries.find((el) =>
      matchedView.hasPreview.includes(el.uri)
    );
    const detailsQuery = configs.detailQueries.find((el) =>
      matchedView.hasDetail.includes(el.uri)
    );

    // reform expansion query
    const das = [];
    if (expansionQuery) {
      expansion = reformQuery(
        expansionQuery,
        "browser:ExpansionQuery",
        matchedView.uri
      );
      das.push(...expansionQuery.hasDataset);
    }

    //reform preview query
    if (previewQuery) {
      preview = reformQuery(
        previewQuery,
        "browser:PreviewQuery",
        matchedView.uri
      );
      das.push(...previewQuery.hasDataset);
    }

    //reform detail query
    if (detailsQuery) {
      detail = reformQuery(
        detailsQuery,
        "browser:DetailQuery",
        matchedView.uri
      );

      das.push(...detailsQuery.hasDataset);
    }

    // parse datasets
    datasets.push(...reformDataSet(configs, Array.from(new Set(das))));

    return {
      view,
      expansion,
      preview,
      detail,
      datasets,
    };
  }
  return null;
};

/**
 * parse a viewset
 * @param {Object} configs
 * @param {Array} matches
 * @param {String} parent
 * @param {Array} selectedViews
 * @returns an array of viewsets
 */
const reformViewsets = (configs, matches, parent, selectedViews = []) => {
  const viewsets = [];
  if (matches.length) {
    const matchedViewsets = configs.viewsets.filter((el) =>
      matches.includes(el.uri)
    );
    if (matchedViewsets.length) {
      viewsets.push(
        ...matchedViewsets.map((el) => ({
          base: { property: "browser:ViewSet", uri: `<${el.uri}>` },
          id: `<${el.uri}>`,
          addedFrom: [parent],
          readOnly: true,
          items: [
            {
              property: "dct:title",
              value: el.title
                ? el.title.map((el) => (el.includes("@") ? el : `"${el}"`))
                : [],
            },
            {
              property: "browser:hasView",
              value: el.hasView
                ? el.hasView
                    .filter((it) =>
                      selectedViews.length
                        ? selectedViews.map((it) => it.id).includes(it)
                        : true
                    )
                    .map((it) => `<${it}>`)
                : [],
            },
            {
              property: "browser:hasDefaultView",
              value: el.hasDefaultView
                ? el.hasDefaultView
                    .filter((it) =>
                      selectedViews.length
                        ? selectedViews.map((it) => it.id).includes(it)
                        : true
                    )
                    .map((it) => `<${it}>`)
                : [],
            },
            {
              property: "browser:hasCondition",
              value: el.hasCondition
                ? el.hasCondition.map((it) => `"""${it}"""`)
                : [],
            },
            {
              property: "browser:hasDataset",
              value: el.hasDataset ? el.hasDataset.map((it) => `<${it}>`) : [],
            },
          ],
        }))
      );
    }
  }
  return viewsets;
};

/**
 * parase a style
 * @param {Object} configs
 * @param {String} matched
 * @param {String} parent
 * @returns an object the contains the style
 */
const reformStyles = (configs, matched, parent) => {
  let style = {};
  const nodeStyles = NODE_STYLES.map((el) => el.name);
  const matchedSt = configs.styles.find((el) => el.uri === matched);

  if (matchedSt) {
    const keys = Object.keys(matchedSt).filter(
      (key) => !["uri", "hasSelector"].includes(key)
    );
    const isNode = keys.every((key) => nodeStyles.includes(key));
    style = {
      base: {
        property: "browser:VisualStyle",
        uri: `<${matchedSt.uri}>`,
      },
      id: `<${matchedSt.uri}>`,
      addedFrom: [parent],
      readOnly: true,
      items: [
        { property: "elementType", value: !isNode },
        {
          property: "browser:hasSelector",
          value: matchedSt["hasSelector"]
            ? [`"${matchedSt["hasSelector"]}"`]
            : [],
        },
        ...keys.map((key) => {
          // check for units
          let withUnit = false;
          let value = "",
            unit = "";
          if (isNode) {
            const node = NODE_STYLES.find(
              (el) => el.name.toLowerCase() === key.toLowerCase()
            );
            if (node && node.units) {
              withUnit = true;
              const matchValues = matchedSt[key][0].match(/\d+/);
              value = matchValues ? matchValues[0] : "";
              unit = matchedSt[key][0].replace(value, "");
            }
          } else {
            const edge = EDGE_STYLES.find(
              (el) => el.name.toLowerCase() === key.toLowerCase()
            );

            if (edge && edge.units) {
              withUnit = true;
              value = matchedSt[key][0].match(/\d+/)[0];
              unit = matchedSt[key][0].replace(value, "");
            }
          }
          if (withUnit) {
            return {
              property: `browser:${key}`,
              value: [value],
              unit: unit,
            };
          }
          return {
            property: `browser:${key}`,
            value: matchedSt[key] ? matchedSt[key].map((el) => `"${el}"`) : [],
          };
        }),
      ],
    };
  }
  return style;
};

/**
 * parse a style sheets
 * @param {Object} configs
 * @param {Array} matches
 * @param {String} parent
 * @param {Array} selectedStyles
 * @returns an array of style sheets
 */
const reformStyleSheets = (configs, matches, parent, selectedStyles = []) => {
  const stylesheet = [];
  if (matches.length) {
    const matchedStylesheet = configs.stylesheet.filter((el) =>
      matches.includes(el.uri)
    );
    if (matchedStylesheet.length) {
      stylesheet.push(
        ...matchedStylesheet.map((el) => ({
          base: {
            property: "browser:VisualStyleSheet",
            uri: `<${el.uri}>`,
          },
          id: `<${el.uri}>`,
          addedFrom: [parent],
          readOnly: true,
          items: [
            {
              property: "browser:hasVisualStyle",
              value: el.hasVisualStyle
                ? el.hasVisualStyle
                    .filter((it) =>
                      selectedStyles.length
                        ? selectedStyles.map((it) => it.id).includes(it)
                        : true
                    )
                    .map((it) => `<${it}>`)
                : [],
            },
          ],
        }))
      );
    }
  }
  return stylesheet;
};
/**
 * convert server response into app state
 * @param {Array} selection
 * @param {Array} fetchedItems
 * @param {String} configBaseLink
 * @returns content list as state
 */
export const convertConfigToState = (
  selection,
  fetchedItems,
  configBaseLink
) => {
  const contentList = {};
  /******* parse datasets ********/

  const selectedDs = Array.from(
    new Set(selection.filter((el) => el.type === "dataset").map((el) => el.id))
  );
  const datasets = [...reformDataSet(fetchedItems, selectedDs)];
  if (datasets.length) {
    contentList["datasets"] = datasets;
  }

  /******* parse views ********/
  const selectedViews = selection.filter((el) => el.type === "view");

  const selectedViewSets = Array.from(
    new Set(selection.filter((el) => el.type === "view").map((el) => el.parent))
  );
  const views = [];
  const expansionQueries = [];
  const previewQueries = [];
  const detailQueries = [];

  if (selectedViews.length) {
    for (const selectedView of selectedViews) {
      const results = reformView(
        fetchedItems,
        selectedView.id,
        selectedView.parent
      );
      if (results) {
        // add the view
        const index = views.findIndex((el) => el.id === results.view.id);
        if (index === -1) {
          views.push(results.view);
        } else {
          const item = views[index];
          views[index] = {
            ...item,
            addedFrom: [...item.addedFrom, selectedView.parent],
          };
        }
        // add the expantion queries
        const qIndex = expansionQueries.findIndex(
          (el) => el.id === results.expansion.id
        );
        if (qIndex === -1) {
          expansionQueries.push(results.expansion);
        } else {
          const item = expansionQueries[qIndex];
          expansionQueries[qIndex] = {
            ...item,
            addedFrom: [...item.addedFrom, results.expansion.addedFrom],
          };
        }
        // add the detail queries
        const dIndex = detailQueries.findIndex(
          (el) => el.id === results.detail.id
        );
        if (dIndex === -1) {
          detailQueries.push(results.detail);
        } else {
          const item = detailQueries[dIndex];
          detailQueries[dIndex] = {
            ...item,
            addedFrom: [...item.addedFrom, results.detail.addedFrom],
          };
        }
        //add the preview quires
        const pIndex = previewQueries.findIndex(
          (el) => el.id === results.preview.id
        );
        if (pIndex === -1) {
          previewQueries.push(results.preview);
        } else {
          const item = previewQueries[pIndex];
          previewQueries[pIndex] = {
            ...item,
            addedFrom: [...item.addedFrom, results.preview.addedFrom],
          };
        }

        // add the data sets
        if (results.datasets.length) {
          for (const ds of results.datasets) {
            const dsIndex = datasets.findIndex((el) => el.id === ds.id);
            if (dsIndex === -1) {
              datasets.push(ds);
            } else {
              const item = datasets[dsIndex];
              datasets[dsIndex] = {
                ...item,
                addedFrom: [...item.addedFrom, ds.addedFrom],
              };
            }
          }
        }
      }
    }
    contentList["views"] = views;
    contentList["previewQueries"] = previewQueries;
    contentList["expansionQueries"] = expansionQueries;
    contentList["detailQueries"] = detailQueries;
    contentList["datasets"] = datasets;
  }
  /******* parse viewsets ********/
  const viewsets = [];
  if (selectedViewSets.length) {
    viewsets.push(
      ...reformViewsets(
        fetchedItems,
        selectedViewSets,
        configBaseLink,
        selectedViews
      )
    );
    contentList["viewsets"] = viewsets;
  }
  /******* parse styles ********/

  const styles = [];

  const selectedSt = selection.filter((el) => el.type === "style");

  const selectedStylesheets = Array.from(
    new Set(
      selection.filter((el) => el.type === "style").map((el) => el.parent)
    )
  );
  if (selectedSt.length) {
    for (const style of selectedSt) {
      const matchedSt = fetchedItems.styles.find((el) => el.uri === style.id);

      if (matchedSt) {
        const reformedSt = reformStyles(
          fetchedItems,
          matchedSt.uri,
          style.parent
        );
        const index = styles.findIndex((el) => el.id === `<${matchedSt.uri}>`);
        if (index === -1) {
          styles.push(reformedSt);
        } else {
          const item = styles[index];
          styles[index] = {
            ...item,
            addedFrom: [...item.addedFrom, reformedSt.parent],
          };
        }
      }
    }

    contentList["styles"] = styles;
  }

  /******* parse stylesheets ********/
  const stylesheet = [];
  if (selectedStylesheets.length) {
    stylesheet.push(
      ...reformStyleSheets(
        fetchedItems,
        selectedStylesheets,
        configBaseLink,
        selectedSt
      )
    );

    contentList["stylesheet"] = stylesheet;
  }

  const configs = [];
  /******* parse configs ********/
  configs.push({
    base: {
      property: "browser:Configuration",
      uri: `<${configBaseLink}>`,
    },
    description: "Configuration section",
    id: "configs",
    readOnly: true,
    items: [
      {
        property: "dct:title",
        value: fetchedItems.configs.title
          ? fetchedItems.configs.title.map((el) =>
              el.includes("@") ? el : `"${el}"`
            )
          : [],
      },
      {
        property: "dct:description",
        value: fetchedItems.configs.description
          ? fetchedItems.configs.description.map((el) =>
              el.includes("@") ? el : `"${el}"`
            )
          : [],
      },
      {
        property: "browser:hasVisualStyleSheet",
        value: selectedStylesheets.map((it) => `<${it}>`),
      },
      {
        property: "browser:startingNode",
        value: fetchedItems.configs.startingNode
          ? fetchedItems.configs.startingNode.map((el) => `<${el}>`)
          : [],
      },
      {
        property: "browser:resourceIriPattern",
        value: fetchedItems.configs.resourceIriPattern
          ? fetchedItems.configs.resourceIriPattern.map((el) => `"${el}"`)
          : [],
      },
      {
        property: "browser:hasViewSet",
        value: selectedViewSets.map((it) => `<${it}>`),
      },
    ],
  });
  contentList["configs"] = configs;

  return contentList;
};

/**
 * attach a compomenet with its related componenets to a state
 * @param {String} type
 * @param {String} uri
 * @param {String} parent
 * @returns new state
 */
export const attachComponent = (type, uri, parent = "") => {
  const contentList = {};
  const views = [];
  const expansionQueries = [];
  const previewQueries = [];
  const detailQueries = [];
  const datasets = [];
  const styles = [];

  const possibleViews = [];
  const possibleDatasets = [];
  const possibleStyles = [];

  const templates = JSON.parse(localStorage.getItem("suggestions"));
  let selectedTemplate = null;
  let selectedComponent = null;
  for (const template of templates) {
    const componenet = template[type];
    if (componenet && componenet.length) {
      const index = componenet.findIndex((el) => el.uri === uri);
      if (index !== -1) {
        selectedTemplate = template;
        selectedComponent = componenet[index];
      }
    }
  }
  // parse view sets
  if (type === "viewsets") {
    contentList.viewsets = reformViewsets(selectedTemplate, [uri], parent);
    possibleViews.push(
      ...selectedComponent.hasView.map((el) => ({
        id: el,
        parent: selectedComponent.uri,
      }))
    );
    possibleDatasets.push(...selectedComponent.hasDataset);
  }

  // parse style sheets
  if (type === "stylesheet") {
    contentList.stylesheet = reformStyleSheets(selectedTemplate, [uri], parent);
    possibleStyles.push(
      ...selectedComponent.hasVisualStyle.map((el) => ({
        id: el,
        parent: selectedComponent.uri,
      }))
    );
  }
  // parse views
  if (type === "views") {
    possibleViews.push({ id: uri, parent: parent });
  }

  if (possibleViews.length) {
    for (const selectedView of possibleViews) {
      const results = reformView(
        selectedTemplate,
        selectedView.id,
        selectedView.parent
      );
      if (results) {
        // add the view
        const index = views.findIndex((el) => el.id === results.view.id);
        if (index === -1) {
          views.push(results.view);
        } else {
          const item = views[index];
          views[index] = {
            ...item,
            addedFrom: [...item.addedFrom, selectedView.parent],
          };
        }
        // add the expantion queries
        const qIndex = expansionQueries.findIndex(
          (el) => el.id === results.expansion.id
        );
        if (qIndex === -1) {
          expansionQueries.push(results.expansion);
        } else {
          const item = expansionQueries[qIndex];
          expansionQueries[qIndex] = {
            ...item,
            addedFrom: [...item.addedFrom, results.expansion.addedFrom],
          };
        }
        // add the detail queries
        const dIndex = detailQueries.findIndex(
          (el) => el.id === results.detail.id
        );
        if (dIndex === -1) {
          detailQueries.push(results.detail);
        } else {
          const item = detailQueries[dIndex];
          detailQueries[dIndex] = {
            ...item,
            addedFrom: [...item.addedFrom, results.detail.addedFrom],
          };
        }
        //add the preview quires
        const pIndex = previewQueries.findIndex(
          (el) => el.id === results.preview.id
        );
        if (pIndex === -1) {
          previewQueries.push(results.preview);
        } else {
          const item = previewQueries[pIndex];
          previewQueries[pIndex] = {
            ...item,
            addedFrom: [...item.addedFrom, results.preview.addedFrom],
          };
        }

        // add the data sets
        if (results.datasets.length) {
          for (const ds of results.datasets) {
            const dsIndex = datasets.findIndex((el) => el.id === ds.id);
            if (dsIndex === -1) {
              datasets.push(ds);
            } else {
              const item = datasets[dsIndex];
              datasets[dsIndex] = {
                ...item,
                addedFrom: [...item.addedFrom, ds.addedFrom],
              };
            }
          }
        }
      }
    }
  }

  // parse styles
  if (type === "styles") {
    possibleStyles.push({ id: uri, parent: parent });
  }
  if (possibleStyles.length) {
    for (const style of possibleStyles) {
      const matchedSt = selectedTemplate.styles.find(
        (el) => el.uri === style.id
      );

      if (matchedSt) {
        const reformedSt = reformStyles(
          selectedTemplate,
          matchedSt.uri,
          style.parent
        );
        const index = styles.findIndex((el) => el.id === `<${matchedSt.uri}>`);
        if (index === -1) {
          styles.push(reformedSt);
        } else {
          const item = styles[index];
          styles[index] = {
            ...item,
            addedFrom: [...item.addedFrom, reformedSt.parent],
          };
        }
      }
    }
  }

  // parse data set
  if (type === "datasets") {
    datasets.push(...reformDataSet(selectedTemplate, [uri]));
  }

  // parse expasion query
  if (type === "expansionQueries") {
    expansionQueries.push(
      reformQuery(selectedComponent, "browser:ExpansionQuery", parent)
    );
  }

  // parse detail query
  if (type === "detailQueries") {
    detailQueries.push(
      reformQuery(selectedComponent, "browser:DetailQuery", parent)
    );
  }

  //parse preview query
  if (type === "previewQueries") {
    previewQueries.push(
      reformQuery(selectedComponent, "browser:PreviewQuery", parent)
    );
  }

  contentList["views"] = views;
  contentList["previewQueries"] = previewQueries;
  contentList["expansionQueries"] = expansionQueries;
  contentList["detailQueries"] = detailQueries;
  contentList["datasets"] = datasets;
  contentList["styles"] = styles;
  return contentList;
};
