import { STYLE_TYPES } from "./constants";
export const NODE_STYLES = [
  {
    name: "width",
    type: STYLE_TYPES.NUMBER,
    units: ["px", "em", "rem", "%"],
  },
  {
    name: "height",
    type: STYLE_TYPES.NUMBER,
    units: ["px", "em", "rem", "%"],
  },
  {
    name: "color",
    type: STYLE_TYPES.COLOR,
  },
  {
    name: "shape",
    type: STYLE_TYPES.SELECT,
    options: [
      "ellipse",
      "triangle",
      "round-triangle",
      "rectangle",
      "round-rectangle",
      "bottom-round-rectangle",
      "cut-rectangle",
      "barrel",
      "rhomboid",
      "diamond",
      "round-diamond",
      "pentagon",
      "round-pentagon",
      "hexagon",
      "round-hexagon",
      "concave-hexagon",
      "heptagon",
      "round-heptagon",
      "octagon",
      "round-octagon",
      "star",
      "tag",
      "round-tag",
      "vee",
    ],
  },
  {
    name: "text-wrap",
    type: STYLE_TYPES.SELECT,
    options: ["none", "ellipsis", "wrap"],
  },
  {
    name: "text-max-width",
    type: STYLE_TYPES.NUMBER,
    units: ["px", "em", "rem", "%"],
  },
  {
    name: "text-overflow-wrap",
    type: STYLE_TYPES.SELECT,
    options: ["whitespace", "anywhere"],
  },
  {
    name: "text-justification",
    type: STYLE_TYPES.SELECT,
    options: ["left", "center", "right", "auto"],
  },
  {
    name: "background-color",
    type: STYLE_TYPES.COLOR,
  },
  {
    name: "background-opacity",
    type: STYLE_TYPES.NUMBER,
  },
  {
    name: "border-width",
    type: STYLE_TYPES.NUMBER,
    units: ["px", "em", "rem", "%"],
  },
  {
    name: "border-style",
    type: STYLE_TYPES.SELECT,
    options: ["solid", "dotted", "dashed", "double"],
  },
  {
    name: "border-opacity",
    type: STYLE_TYPES.NUMBER,
  },
  {
    name: "border-color",
    type: STYLE_TYPES.COLOR,
  },
  {
    name: "padding",
    type: STYLE_TYPES.NUMBER,
    units: ["px", "em", "rem", "%"],
  },
  {
    name: "label",
    type: STYLE_TYPES.TEXT,
  },
  {
    name: "font-size",
    type: STYLE_TYPES.NUMBER,
    units: ["px", "em", "rem", "%"],
  },
  {
    name: "text-halign",
    type: STYLE_TYPES.SELECT,
    options: ["left", "center", "right"],
  },
  {
    name: "text-valign",
    type: STYLE_TYPES.SELECT,
    options: ["top", "center", "bottom"],
  },
  {
    name: "text-margin-x",
    type: STYLE_TYPES.NUMBER,
    units: ["px", "em", "rem", "%"],
  },
  {
    name: "text-margin-y",
    type: STYLE_TYPES.NUMBER,
    units: ["px", "em", "rem", "%"],
  },
  {
    name: "text-outline-color",
    type: STYLE_TYPES.COLOR,
  },
  {
    name: "text-outline-opacity",
    type: STYLE_TYPES.NUMBER,
  },
  {
    name: "text-outline-width",
    type: STYLE_TYPES.NUMBER,
    units: ["px", "em", "rem", "%"],
  },
  {
    name: "opacity",
    type: STYLE_TYPES.NUMBER,
  },
  {
    name: "visibility",
    type: STYLE_TYPES.SELECT,
    options: ["hidden", "visible"],
  },
  {
    name: "z-index",
    type: STYLE_TYPES.NUMBER,
  },
  {
    name: "text-transform",
    type: STYLE_TYPES.SELECT,
    options: ["none", "uppercase", "lowercase"],
  },
];

export const EDGE_STYLES = [
  {
    name: "width",
    type: STYLE_TYPES.NUMBER,
    units: ["px", "em", "rem", "%"],
  },
  {
    name: "curve-style",
    type: STYLE_TYPES.SELECT,
    options: [
      "haystack",
      "straight",
      "bezier",
      "unbundled-bezier",
      "segments",
      "taxi",
    ],
  },
  {
    name: "color",
    type: STYLE_TYPES.COLOR,
  },
  {
    name: "text-outline-color",
    type: STYLE_TYPES.COLOR,
  },
  {
    name: "line-color",
    type: STYLE_TYPES.COLOR,
  },
  {
    name: "line-style",
    type: STYLE_TYPES.SELECT,
    options: ["solid", "dotted", "dashed"],
  },
  {
    name: "line-cap",
    type: STYLE_TYPES.SELECT,
    options: ["butt", "round", "square"],
  },
  {
    name: "label",
    type: STYLE_TYPES.TEXT,
  },
  {
    name: "font-size",
    type: STYLE_TYPES.NUMBER,
    units: ["px", "em", "rem", "%"],
  },
  {
    name: "text-outline-width",
    type: STYLE_TYPES.NUMBER,
    units: ["px", "em", "rem", "%"],
  },
  {
    name: "text-outline-opacity",
    type: STYLE_TYPES.NUMBER,
  },
  {
    name: "target-arrow-color",
    type: STYLE_TYPES.COLOR,
  },
  {
    name: "control-point-step-size",
    type: STYLE_TYPES.NUMBER,
  },
  {
    name: "control-point-distance",
    type: STYLE_TYPES.NUMBER,
  },
  {
    name: "control-point-weigh",
    type: STYLE_TYPES.NUMBER,
  },

  {
    name: "target-arrow-shape",
    type: STYLE_TYPES.SELECT,
    options: [
      "triangle",
      "triangle-tee",
      "circle-triangle",
      "triangle-cross",
      "triangle-backcurve",
      "vee",
      "tee",
      "square",
      "circle",
      "diamond",
      "chevron",
      "none",
    ],
  },
  {
    name: "source-arrow-color",
    type: STYLE_TYPES.COLOR,
  },
  {
    name: "source-arrow-shape",
    type: STYLE_TYPES.SELECT,
    options: [
      "triangle",
      "triangle-tee",
      "circle-triangle",
      "triangle-cross",
      "triangle-backcurve",
      "vee",
      "tee",
      "square",
      "circle",
      "diamond",
      "chevron",
      "none",
    ],
  },
  {
    name: "mid-source-arrow-color",
    type: STYLE_TYPES.COLOR,
  },
  {
    name: "mid-source-arrow-shape",
    type: STYLE_TYPES.SELECT,
    options: [
      "triangle",
      "triangle-tee",
      "circle-triangle",
      "triangle-cross",
      "triangle-backcurve",
      "vee",
      "tee",
      "square",
      "circle",
      "diamond",
      "chevron",
      "none",
    ],
  },
  {
    name: "mid-target-arrow-color",
    type: STYLE_TYPES.COLOR,
  },
  {
    name: "mid-target-arrow-shape",
    type: STYLE_TYPES.SELECT,
    options: [
      "triangle",
      "triangle-tee",
      "circle-triangle",
      "triangle-cross",
      "triangle-backcurve",
      "vee",
      "tee",
      "square",
      "circle",
      "diamond",
      "chevron",
      "none",
    ],
  },
  {
    name: "opacity",
    type: STYLE_TYPES.NUMBER,
  },
  {
    name: "visibility",
    type: STYLE_TYPES.SELECT,
    options: ["hidden", "visible"],
  },
  {
    name: "z-index",
    type: STYLE_TYPES.NUMBER,
  },
  {
    name: "text-transform",
    type: STYLE_TYPES.SELECT,
    options: ["none", "uppercase", "lowercase"],
  },
];
