export const SECTION_SEPARATOR = "# End Of Section #";
export const LINE_SEPARATOR = "#########";
export const EDITOR_PREFIX = "editor-";
export const PFX_PREFIX = "pfx-";
export const SECTION_PREFIX = "section-";
export const NODE_PREFIX = "node-";
export const NODE_TYPES = {
  TEXT: "text",
  LINK: "link",
  EDITOR: "editor",
  SELECT: "select",
};

export const STYLE_TYPES = {
  NUMBER: "number",
  SELECT: "select",
  COLOR: "color",
  TEXT: "text",
};

export const VOCAB_TYPES = {
  CLASS: "owl:Class",
  OBJECT_PROPERTY: "owl:ObjectProperty",
  DATA_TYPE_PROPERTY: "owl:DatatypeProperty",
};
