import { NODE_TYPES } from "./constants";
const NODES = {
  CONFIGURATIONS: [
    {
      title: "Title",
      nodeName: "dct:title",
      type: NODE_TYPES.TEXT,
      example: '"title goes here"@lang',
      multi: true,
      supportLang: true,
    },
    {
      title: "Description",
      nodeName: "dct:description",
      type: NODE_TYPES.TEXT,
      example: '"description goes here"@lang',
      multi: true,
      supportLang: true,
    },
    {
      title: "Stylesheets",
      nodeName: "browser:hasVisualStyleSheet",
      type: NODE_TYPES.LINK,
      linkedTo: "stylesheet",
      linkedToName: "browser:VisualStyleSheet",
      example:
        "https://linked.opendata.cz/resource/knowledge-graph-browser/wikidata/people/style-sheet",
      withSuggestions: true,
      multi: true,
    },
    {
      title: "Starting Node",
      nodeName: "browser:startingNode",
      type: NODE_TYPES.LINK,
      example: "http://www.wikidata.org/entity/Q80",
      multi: true,
    },
    {
      title: "IRI Pattern",
      nodeName: "browser:resourceIriPattern",
      type: NODE_TYPES.TEXT,
      example: '"^http://www\\.wikidata\\.org/entity/Q[1-9][0-9]*$"',
      multi: false,
      supportLang: false,
    },
    {
      title: "ViewSet",
      nodeName: "browser:hasViewSet",
      type: NODE_TYPES.LINK,
      linkedTo: "viewsets",
      linkedToName: "browser:ViewSet",
      example:
        "https://linked.opendata.cz/resource/knowledge-graph-browser/view-set/wikidata/person",
      withSuggestions: true,
      multi: true,
    },
  ],

  VIEWSET: [
    {
      title: "Title",
      nodeName: "dct:title",
      type: NODE_TYPES.TEXT,
      example: '"title goes here"@lang',
      multi: true,
      supportLang: true,
    },
    {
      title: "Views",
      nodeName: "browser:hasView",
      type: NODE_TYPES.LINK,
      linkedTo: "views",
      linkedToName: "browser:View",
      example:
        "https://linked.opendata.cz/resource/knowledge-graph-browser/view/wikidata/person/relatives/parents",
      withSuggestions: true,
      multi: true,
    },
    {
      title: "Default View",
      nodeName: "browser:hasDefaultView",
      type: NODE_TYPES.SELECT,
      optionsSource: "browser:hasView",
      options: [],
      example:
        "https://linked.opendata.cz/resource/knowledge-graph-browser/view/wikidata/person/relatives/parents",
      multi: false,
    },
    {
      title: "Condition",
      nodeName: "browser:hasCondition",
      type: NODE_TYPES.EDITOR,
    },
    {
      title: "Dataset",
      nodeName: "browser:hasDataset",
      type: NODE_TYPES.LINK,
      example:
        "https://linked.opendata.cz/resource/knowledge-graph-browser/dataset/wikidata",
      withSuggestions: true,
      multi: false,
    },
  ],
  VIEW: [
    {
      title: "Title",
      nodeName: "dct:title",
      type: NODE_TYPES.TEXT,
      example: '"title goes here"@lang',
      multi: true,
      supportLang: true,
    },
    {
      title: "Description",
      nodeName: "dct:description",
      type: NODE_TYPES.TEXT,
      example: '"description goes here"@lang',
      multi: true,
      supportLang: true,
    },
    {
      title: "Expansion Query",
      nodeName: "browser:hasExpansion",
      type: NODE_TYPES.LINK,
      linkedTo: "expansionQueries",
      linkedToName: "browser:ExpansionQuery",
      example:
        "https://linked.opendata.cz/resource/knowledge-graph-browser/expansion-query/wikidata/screenwriter/basic>",
      withSuggestions: true,
      multi: false,
    },
    {
      title: "Preview Query",
      nodeName: "browser:hasPreview",
      type: NODE_TYPES.LINK,
      linkedTo: "previewQueries",
      linkedToName: "browser:PreviewQuery",
      example:
        "https://linked.opendata.cz/resource/knowledge-graph-browser/preview-query/wikidata/creative-work/basic",
      withSuggestions: true,
      multi: false,
    },
    {
      title: "Detail Query",
      nodeName: "browser:hasDetail",
      type: NODE_TYPES.LINK,
      linkedTo: "detailQueries",
      linkedToName: "browser:DetailQuery",
      example:
        "https://linked.opendata.cz/resource/knowledge-graph-browser/detail-query/wikidata/creative-work/basic>",
      withSuggestions: true,
      multi: false,
    },
  ],
  QUERY: [
    {
      title: "Title",
      nodeName: "dct:title",
      type: NODE_TYPES.TEXT,
      example: '"title goes here"@lang',
      multi: true,
      supportLang: true,
    },
    {
      title: "Dataset",
      nodeName: "browser:hasDataset",
      type: NODE_TYPES.LINK,
      linkedTo: "datasets",
      linkedToName: "void:Dataset",
      example:
        "https://linked.opendata.cz/resource/knowledge-graph-browser/dataset/wikidata",
      withSuggestions: true,
      multi: false,
    },
    {
      title: "Query",
      nodeName: "browser:query",
      type: NODE_TYPES.EDITOR,
      withPreview: true,
    },
  ],
  DATASET: [
    {
      title: "Title",
      nodeName: "dct:title",
      type: NODE_TYPES.TEXT,
      example: '"title goes here"@lang',
      multi: true,
      supportLang: true,
    },
    {
      title: "SPARQL Endpoint",
      nodeName: "void:sparqlEndpoint",
      type: NODE_TYPES.LINK,
      example: "https://query.wikidata.org/sparql",
      multi: false,
    },
    {
      title: "Accept",
      nodeName: "browser:accept",
      type: NODE_TYPES.TEXT,
      example: '"application/sparql-results+json"',
      multi: false,
    },
  ],
  STYLESHEET: [
    {
      title: "Styles",
      nodeName: "browser:hasVisualStyle",
      type: NODE_TYPES.LINK,
      linkedTo: "styles",
      linkedToName: "browser:VisualStyle",
      example:
        "https://linked.opendata.cz/resource/knowledge-graph-browser/wikidata/people/style/human",
      withSuggestions: true,
      multi: true,
    },
  ],
  VOCAB_CLASS: [
    {
      title: "Label",
      nodeName: "rdfs:label",
      type: NODE_TYPES.TEXT,
      example: '"label goes here"@lang',
      multi: true,
      supportLang: true,
    },
    {
      title: "Comment",
      nodeName: "rdfs:comment",
      type: NODE_TYPES.TEXT,
      example: '"comment goes here"@lang',
      multi: true,
      supportLang: true,
    },
  ],

  VOCAB_PROPERTY: [
    {
      title: "Label",
      nodeName: "rdfs:label",
      type: NODE_TYPES.TEXT,
      example: '"label goes here"@lang',
      multi: true,
      supportLang: true,
    },
    {
      title: "Comment",
      nodeName: "rdfs:comment",
      type: NODE_TYPES.TEXT,
      example: '"comment goes here"@lang',
      multi: true,
      supportLang: true,
    },
    {
      title: "Domain",
      nodeName: "rdfs:domain",
      type: NODE_TYPES.TEXT,
      example: "wdab:entity",
      multi: false,
      supportLang: false,
      hasPrefix: true,
    },
    {
      title: "Range",
      nodeName: "rdfs:range",
      type: NODE_TYPES.TEXT,
      example: "rdfs:Literal",
      multi: false,
      supportLang: false,
      hasPrefix: true,
    },
  ],
};

export default NODES;
