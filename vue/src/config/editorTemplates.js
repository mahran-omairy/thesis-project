export const EDITOR_TEMPLATE = {
  expansionQueries: ` #PREFIX pfx: <http://sample.uri/>
  ################# END OF PREFIXES ##################
  CONSTRUCT {
    ##### CONSTRUCT OF EXPANTION NODES #####
    # ?expansionNode a <uri> ;
    #   rdfs:label ?expansionNodeLabel ;
    #   visualStyle:class ?expansionNodeClass .
      
    ##### CONSTRUCT OF EXPANTION EDGES #####
    # ?node has:edgeTo ?expansionNode .
    
    ##### CONSTRUCT VISUAL STYLES OF EDGES #####
    # has:edgeTo visualStyle:class "className" .  

  ################# END OF CONSTRUCT ##################
  } WHERE {
  
    ##### PATTERNS FOR EXTRACTING THE NODES ####
    
  }`,
  detailQueries: ` #PREFIX pfx: <http://sample.uri/>
  ################# END OF PREFIXES ##################
  CONSTRUCT {
    ##### CONSTRUCT OF NODE WITH LABELS #####
    # ?node rdfs:label ?nodeLabel ;
    #  label:mandatory ?mlbl ;
    #  label:optional ?olbl .

  } WHERE {
    ##### EXTRACT MANDATORY LABELS #####
    #  ?node label:mandatory ?mlbl .

    ##### EXTRACT OPTIONAL LABELS #####
    #  ?node label:optional ?olbl .
  }`,
  previewQueries: ` #PREFIX pfx: <http://sample.uri/>
  ################# END OF PREFIXES ##################
  CONSTRUCT {
    ##### CONSTRUCT OF NODE WITH LABELS AND VISUAL CLASSES #####
    # ?node a <uri> ;
    #   rdfs:label ?nodeLabel ;
    #   visualStyle:class ?nodeClass .
  } WHERE {
    ##### EXTRACT NODE LABELS AND VISUAL CLASSES #####
    #   ?node label:uri ?labelValue .
    
  }`,
};
