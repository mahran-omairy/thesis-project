const mutations = {
  updateContentList(state, payload) {
    state.contentList = { ...payload };
  },
  // updateFileName(state, payload) {
  //   state.fileName = payload;
  // },
  updateBaseLink(state, payload) {
    state.baseLink = payload;
  },

  updateSeletedTemplates(state, payload) {
    state.selectedTemplates = payload;
  },
  preparePreview(state, payload) {
    state.queryPreview = payload;
  },
};

export default mutations;
