const state = {
  contentList: {
    prefixes: [],
    configs: [],
    viewsets: [],
    views: [],
    expansionQueries: [],
    previewQueries: [],
    detailQueries: [],
    datasets: [],
    stylesheet: [],
    styles: [],
    vocabs: [],
  },
  // fileName: "",
  baseLink: "",

  queryPreview: {
    type: "",
    query: "",
    init: false,
  },

  selectedTemplates: [],
};

export default state;
