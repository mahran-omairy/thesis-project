const mutations = {
  updateContentList({ commit }, payload) {
    commit("updateContentList", payload);
  },
  updateBaseLink({ commit }, payload) {
    commit("updateBaseLink", payload);
  },
  updateSeletedTemplates({ commit }, payload) {
    commit("updateSeletedTemplates", payload);
  },
  preparePreview({ commit }, payload) {
    commit("preparePreview", payload);
  },
};

export default mutations;
