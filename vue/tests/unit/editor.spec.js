import Vue from "vue";
import Vuetify from "vuetify";
import { mount, createLocalVue } from "@vue/test-utils";
import Editor from "@/components/shared/Editor.vue";

Vue.use(Vuetify);

describe("Editor.vue", () => {
  const localVue = createLocalVue();
  let vuetify;

  beforeEach(() => {
    vuetify = new Vuetify();
  });

  it("will not render yasgui off screen", () => {
    const id = "editor-testing",
      editorId = "editor-1";
    const wrapper = mount(Editor, {
      localVue,
      vuetify,
      propsData: { id, editorId },
    });
    expect(wrapper.find("#" + id + editorId).exists()).toBe(true);
    expect(wrapper.vm.yasgui).toBe(null);
  });

  it("renders disabled textarea", () => {
    const id = "editor-testing",
      editorId = "editor-1";
    const wrapper = mount(Editor, {
      localVue,
      vuetify,
      propsData: { id, editorId, readOnly: true },
    });
    expect(wrapper.find("#" + id + editorId).exists()).toBe(false);
    expect(wrapper.find(".v-textarea").exists()).toBe(true);
    expect(
      wrapper
        .find(".v-textarea")
        .classes()
        .includes("v-input--is-readonly")
    ).toBe(true);
  });

  it("renders editor with preview button", () => {
    const id = "editor-testing",
      editorId = "editor-1";
    const wrapper = mount(Editor, {
      localVue,
      vuetify,
      propsData: { id, editorId, readOnly: true, withPreview: true },
    });
    expect(wrapper.find(".v-textarea").exists()).toBe(true);

    expect(wrapper.find(".v-btn").exists()).toBe(true);
  });
});
