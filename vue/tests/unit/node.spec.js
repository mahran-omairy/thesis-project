import Vue from "vue";
import Vuetify from "vuetify";
import { mount, createLocalVue } from "@vue/test-utils";
import Node from "@/components/shared/node.vue";
import { NODE_TYPES } from "../../src/config/constants";

Vue.use(Vuetify);

describe("node.vue", () => {
  const localVue = createLocalVue();
  let vuetify;

  beforeEach(() => {
    vuetify = new Vuetify();
  });

  it("renders title, and ediatble input", () => {
    const node = { type: NODE_TYPES.TEXT, title: "View" };

    const wrapper = mount(Node, {
      localVue,
      vuetify,
      propsData: { node },
    });
    expect(wrapper.find(".col.col-2").text()).toMatch(node.title);
    expect(wrapper.find(".v-input").exists()).toBe(true);
    expect(
      wrapper
        .find(".v-input")
        .classes()
        .includes("v-input--is-readonly")
    ).toBe(false);
  });

  it("renders disabled", () => {
    const node = { type: NODE_TYPES.TEXT, title: "View" };

    const wrapper = mount(Node, {
      localVue,
      vuetify,
      propsData: { readOnly: true, node },
    });
    expect(
      wrapper
        .find(".v-input")
        .classes()
        .includes("v-input--is-readonly")
    ).toBe(true);
  });

  it("renders with multi support", () => {
    const node = { type: NODE_TYPES.LINK, title: "View", multi: true };

    const wrapper = mount(Node, {
      localVue,
      vuetify,
      propsData: { node },
    });
    expect(wrapper.find(".col.col-2").text()).toMatch(node.title);
    expect(wrapper.find(".v-input").exists()).toBe(true);
    expect(wrapper.find(".v-btn").exists()).toBe(true);
  });

  it("renders with filled values", () => {
    const node = { type: NODE_TYPES.TEXT, title: "View", multi: true };
    const value = ["testing title", "testing title 2"];
    const wrapper = mount(Node, {
      localVue,
      vuetify,
      propsData: { node, value },
    });

    expect(wrapper.find(".col.col-2").text()).toMatch(node.title);
    expect(wrapper.vm.fields.length).toBe(value.length);
  });

  it("renders select", () => {
    const node = { type: NODE_TYPES.SELECT };
    const wrapper = mount(Node, {
      localVue,
      vuetify,
      propsData: { node },
    });

    expect(wrapper.find(".v-select").exists()).toBe(true);
  });
});
