import Vue from "vue";
import Vuetify from "vuetify";
import { mount, createLocalVue } from "@vue/test-utils";
import SuggestionPanel from "@/components/shared/SuggestionPanel.vue";

Vue.use(Vuetify);

describe("SuggestionPanel.vue", () => {
  const localVue = createLocalVue();
  let vuetify;

  beforeEach(() => {
    vuetify = new Vuetify();
  });

  it("renders menu with 3 items", () => {
    const items = [
      { id: "item 1", title: "title 1" },
      { id: "item 2", title: "title 2" },
      { id: "item 3", title: "title 3" },
    ];
    const wrapper = mount(SuggestionPanel, {
      localVue,
      vuetify,
      propsData: { items },
    });

    expect(wrapper.findAll(".v-list-item").length).toBe(items.length);
  });

  it("renders menu with 2 items", () => {
    const items = [
      { id: "item 1", title: "title 1" },
      { id: "item 2", title: "title 2" },
      { id: "item 3", title: "title 3" },
    ];
    const execlude = ["item 3"];

    const wrapper = mount(SuggestionPanel, {
      localVue,
      vuetify,
      propsData: { items, execlude },
    });

    expect(wrapper.findAll(".v-list-item").length).toBe(
      items.length - execlude.length
    );
  });

  it("emit an event", () => {
    const items = [
      { id: "item 1", title: "title 1" },
      { id: "item 2", title: "title 2" },
      { id: "item 3", title: "title 3" },
    ];
    const execlude = ["item 3"];

    const wrapper = mount(SuggestionPanel, {
      localVue,
      vuetify,
      propsData: { items, execlude },
    });

    const payload = { value: "item 2", parentId: "parent-id" };
    wrapper.vm.$emit("onSelect", payload);

    expect(wrapper.emitted().onSelect).toBeTruthy();
    expect(wrapper.emitted().onSelect[0][0]).toEqual(payload);
  });
});
