import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { FileListDto } from './dto/file.list.dto';
import { File } from './file.entiry';

@Injectable()
export class FilesService {
  constructor(
    @InjectRepository(File)
    private fileRepository: Repository<File>,
  ) {}

  /**
   * List all files
   * @returns Promise<FileListDto[]>
   */
  async findAll(): Promise<FileListDto[]> {
    const result = await this.fileRepository
      .createQueryBuilder()
      .select(['id', 'name', 'created_at', 'updated_at'])
      .execute();
    return result.map(el => ({
      id: el['id'],
      name: el['name'],
      createdAt: el['created_at'],
      updatedAt: el['updated_at'],
    }));
  }

  /**
   * find a file by its id
   * @param id Number
   * @returns Promise<File>
   */
  async findOne(id: number): Promise<File> {
    return await this.fileRepository.findOne(id);
  }

  /**
   * remove a file by its id
   * @param id Number
   */
  async remove(id: number): Promise<void> {
    await this.fileRepository.delete(id);
  }

  /**
   * save a new file and return the content
   * @param query File
   * @returns Promise<File>
   */
  async create(query: File): Promise<File> {
    return await this.fileRepository.save(query);
  }
  /**
   * update an exisiting file and return its content
   * @param id Number
   * @param query File
   * @returns Promise<File>
   */
  async update(id: number, query: File): Promise<File> {
    await this.fileRepository.update({ id }, { ...query });
    return await this.fileRepository.findOne(id);
  }
}
