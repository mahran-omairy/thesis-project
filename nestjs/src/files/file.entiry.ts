import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

@Entity()
export class File {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @Column({ nullable: false })
  name: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  @Column({ nullable: true })
  baseLink: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  @Column({ type: 'text', nullable: true })
  metaGroup: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @Column({ type: 'longtext', nullable: false })
  body: string;

  @ApiProperty()
  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @ApiProperty()
  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;
}
