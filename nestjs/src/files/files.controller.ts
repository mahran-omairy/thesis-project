import {
  Body,
  Query,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { ApiOperation, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { FilesService } from './files.service';
import { File } from './file.entiry';
import { FileListDto } from './dto/file.list.dto';

@ApiTags('Files')
@Controller('v1/files')
export class FilesController {
  constructor(private filesService: FilesService) {}

  /**
   * list avilable files
   * @returns Promise<FileListDto[]>
   */
  @ApiOperation({ summary: 'List saved configuration files' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'List of saved configuration files',
    type: FileListDto,
  })
  @Get()
  async list(): Promise<FileListDto[]> {
    return await this.filesService.findAll();
  }

  /**
   * save a new file and return the content
   * @param query File
   * @returns Promise<File>
   */
  @ApiOperation({ summary: 'Save a configuration file' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'File was saved',
    type: File,
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Could not create the file',
  })
  @Post()
  async create(@Body() file: File): Promise<File> {
    return await this.filesService.create(file);
  }

  /**
   * find a file by its id
   * @param id Number
   * @returns Promise<File>
   */
  @ApiOperation({ summary: 'Get a saved file by id' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Save configuration file',
    type: File,
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'File not found',
  })
  @Get(':id')
  async get(@Param('id') id: number): Promise<File> {
    const item = await this.filesService.findOne(id);
    if (!item) {
      throw new HttpException('Item not found', HttpStatus.NOT_FOUND);
    }
    return item;
  }

  /**
   * remove a file by its id
   * @param id Number
   */
  @ApiOperation({ summary: 'Delete a saved file by id' })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'File was deleted',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'File not found',
  })
  @Delete(':id')
  @HttpCode(204)
  async delete(@Param('id') id: number): Promise<void> {
    const item = await this.filesService.findOne(id);
    if (!item) {
      throw new HttpException('Item not found', HttpStatus.NOT_FOUND);
    }
    this.filesService.remove(id);
  }

  /**
   * update an exisiting file and return its content
   * @param id Number
   * @param query File
   * @returns Promise<File>
   */
  @ApiOperation({ summary: 'Update a saved file by id' })
  @ApiResponse({
    status: HttpStatus.ACCEPTED,
    description: 'File was updated',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'File not found',
  })
  @Put(':id')
  @HttpCode(202)
  async update(@Param('id') id: number, @Body() file: File): Promise<File> {
    const item = await this.filesService.findOne(id);
    if (!item) {
      throw new HttpException('Item not found', HttpStatus.NOT_FOUND);
    }
    return this.filesService.update(id, file);
  }
}
