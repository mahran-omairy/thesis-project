import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

class dataSetDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  sparqlEndpoint: string;
  @ApiProperty()
  @IsString()
  @IsOptional()
  accept: string;
}

export class QueryBodyDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  sparql: string;
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  resource: string;

  @ApiProperty()
  @IsNotEmpty()
  dataset: dataSetDto;
}

export class MetaDto {
  @ApiProperty()
  @IsNotEmpty()
  metaList: string[];
}
