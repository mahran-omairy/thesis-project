import { HttpModule, Module } from '@nestjs/common';
import { QueriesService } from './qureies.service';
import { QueriesController } from './queries.controller';

@Module({
  imports: [HttpModule],
  providers: [QueriesService],
  controllers: [QueriesController],
})
export class QueriesModule {}
