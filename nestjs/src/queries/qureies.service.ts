import {
  HttpException,
  HttpService,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { QueryBodyDto } from './dto/query.body.dto';

@Injectable()
export class QueriesService {
  constructor(private httpService: HttpService) {}

  baseUrl = process.env.KG_SERVER_HOST || 'http://localhost:3000/';
  /**
   * run a query based on its type
   * @param type string
   * @param body QueryBodyDto
   * @returns Promise<any> the query result
   */
  async executeQuery(type: String, body: QueryBodyDto): Promise<any> {
    try {
      const resutl = await this.httpService
        .post(this.baseUrl + type, body)
        .toPromise()
        .then(data => data);

      return resutl.data;
    } catch (e) {
      throw new HttpException(
        'could not load the query',
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  /**
   * fetch the configuration file conponents
   * @param uri string
   * @returns Promise<any> the configuration file components
   */
  async fetchInfo(uri: string): Promise<any> {
    const fullSparql = {
      configs: {
        uri: '',
        description: [],
        title: [],
        hasViewSet: [],
        hasVisualStyleSheet: [],
        resourceIriPattern: [],
        startingNode: [],
      },
      viewsets: [],
      views: [],
      expansionQueries: [],
      previewQueries: [],
      detailQueries: [],
      datasets: [],
      vocabs: [],
      stylesheet: [],
      styles: [],
    };
    const config = await this.fetchParsedInfo(uri);
    if (!config) {
      return null;
    }

    // parse the data
    fullSparql.configs = { uri: uri, ...this.extractValues(config) };

    // get all viewsets
    if (fullSparql.configs.hasViewSet?.length) {
      fullSparql.viewsets = await this.fetchSection(
        fullSparql.configs.hasViewSet,
      );
    }

    // get style sheets
    if (fullSparql.configs.hasVisualStyleSheet?.length) {
      fullSparql.stylesheet = await this.fetchSection(
        fullSparql.configs.hasVisualStyleSheet,
      );
    }

    // fetch viewset into
    if (fullSparql.viewsets?.length) {
      const views = [];
      const datasets = [];

      for (const viewset of fullSparql.viewsets) {
        //get view Sets
        if (viewset.hasView?.length) {
          const data = await this.fetchSection(viewset.hasView);
          for (const item of data) {
            if (views.findIndex(el => el.uri === item.uri) === -1) {
              views.push(item);
            }
          }
        }
        // get datasets
        if (viewset.hasDataset?.length) {
          const data = await this.fetchSection(viewset.hasDataset);
          for (const item of data) {
            if (datasets.findIndex(el => el.uri === item.uri) === -1) {
              datasets.push(item);
            }
          }
        }
      }
      fullSparql.views = views;
      fullSparql.datasets = datasets;
    }

    // fetch views info
    if (fullSparql.views?.length) {
      const detail = [];
      const expansion = [];
      const preview = [];

      for (const view of fullSparql.views) {
        //get detail queries
        if (view.hasDetail?.length) {
          const data = await this.fetchSection(view.hasDetail);
          for (const item of data) {
            if (detail.findIndex(el => el.uri === item.uri) === -1) {
              detail.push(item);
            }
          }
        }
        // get expansion queries
        if (view.hasExpansion?.length) {
          const data = await this.fetchSection(view.hasExpansion);
          for (const item of data) {
            if (expansion.findIndex(el => el.uri === item.uri) === -1) {
              expansion.push(item);
            }
          }
        }
        // get preview queries
        if (view.hasPreview?.length) {
          const data = await this.fetchSection(view.hasPreview);
          for (const item of data) {
            if (preview.findIndex(el => el.uri === item.uri) === -1) {
              preview.push(item);
            }
          }
        }
      }

      fullSparql.expansionQueries = expansion;
      fullSparql.previewQueries = preview;
      fullSparql.detailQueries = detail;
    }

    // fetch styles
    if (fullSparql.stylesheet?.length) {
      const styles = [];

      for (const stylesheet of fullSparql.stylesheet) {
        //get styles
        if (stylesheet.hasVisualStyle?.length) {
          const data = await this.fetchSection(stylesheet.hasVisualStyle);
          for (const item of data) {
            if (styles.findIndex(el => el.uri === item.uri) === -1) {
              styles.push(item);
            }
          }
        }
      }
      fullSparql.styles = styles;
    }

    return fullSparql;
  }

  /**
   * convert an resonse from sparql endpoint into something can be used int eh front end
   * @param object
   * @returns parsedObject
   */
  extractValues(object: any): any {
    const parsedObject = {};
    Object.keys(object).forEach(key => {
      const keyParts = key.split('/');
      const actualKey = keyParts[keyParts.length - 1];
      if (object[key]?.length && !actualKey.includes('#type')) {
        parsedObject[actualKey] = object[key].map(el => {
          if (el.lang) {
            return `"${el.value}"@${el.lang}`;
          }
          return el.value;
        });
      }
    });
    return parsedObject;
  }

  /**
   * encode a url that contains some special characters
   * @param source String
   * @returns String
   */
  getFetchableURI(source: string): string {
    let state = 0;
    let chars = 0;
    let value = '';
    let result = '';

    for (let i = 0; i < source.length; i++) {
      switch (state) {
        case 0:
          if (source.charAt(i) == '\\') {
            state = 1;
          } else {
            result += source.charAt(i);
          }
          break;
        case 1:
          if (source.charAt(i) == 'u') {
            state = 2;
            chars = 0;
            value = '';
          } else {
            result += '\\' + source.charAt(i);
            state = 0;
          }
          break;
        case 2:
          chars++;
          value += source.charAt(i);
          if (chars >= 4) {
            result += unescape('%u' + value);
            state = 0;
          }
          break;
      }
    }

    let pattern = new RegExp(/(http(s)?:\/\/([^\/]+)\/)(.*)/g);
    let e = result.replace(pattern, '$4');
    let b = result.replace(pattern, '$1');

    return b + encodeURI(e);
  }

  /**
   * fetch a LOD resource using an http call with a application/json response
   * @param uri
   * @returns Promise<any>
   */
  async fetchParsedInfo(uri: string): Promise<any> {
    const res = await this.httpService
      .get(this.getFetchableURI(uri), {
        headers: {
          Accept: 'application/json',
        },
      })
      .toPromise()
      .then(data => data);
    return res.data[uri];
  }

  /**
   * collect the data about a list of components
   * @param items array
   * @returns Promise<any>
   */
  async fetchSection(items: any): Promise<any> {
    const builtData = [];
    for (const itemUri of items) {
      const item = await this.fetchParsedInfo(itemUri);
      builtData.push({ uri: itemUri, ...this.extractValues(item) });
    }
    return builtData;
  }

  /**
   * fetch the information about the meta configurations list from kgbrowser
   * @param iri string
   * @returns Promise<any>
   */
  async fetchMeta(iri: string): Promise<any> {
    // start with the super group
    try {
      const result = [];
      const res = await this.httpService
        .get(`${this.baseUrl}meta-configuration`, {
          headers: {
            Accept: 'application/json',
          },
          params: { iri, languages: 'en' },
        })
        .toPromise()
        .then(data => data);
      // for each meta group load information about it's meta items
      for (const metaConfig of res.data.has_meta_configurations) {
        const metaRes = await this.httpService
          .get(`${this.baseUrl}meta-configuration`, {
            headers: {
              Accept: 'application/json',
            },
            params: { iri: metaConfig.iri, languages: 'en' },
          })
          .toPromise()
          .then(data => data);
        result.push({
          iri: metaConfig.iri,
          title: metaConfig.title.en,
          configs: metaRes.data.has_configurations
            .map(el => ({
              iri: el.iri,
              title: el.title.en,
            }))
            .sort((a, b) => {
              let nameA = a.title.toUpperCase();
              let nameB = b.title.toUpperCase();
              if (nameA < nameB) {
                return -1;
              }
              if (nameA > nameB) {
                return 1;
              }
              return 0;
            }),
        });
      }
      // sort items alphabetically
      return result.sort((a, b) => {
        let nameA = a.title.toUpperCase();
        let nameB = b.title.toUpperCase();
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }
        return 0;
      });
    } catch (e) {
      console.log(e);
    }
    return null;
  }

  /**
   * load componenets of a list of meta configurations
   * @param iris string[]
   * @returns Promise<any>
   */

  async fetchSuggestions(iris: string[]): Promise<any> {
    try {
      const suggestios = [];

      for (const iri of iris) {
        const data = await this.fetchInfo(iri);

        suggestios.push(data);
      }

      return suggestios;
    } catch (e) {
      throw new HttpException(
        'could not load the list',
        HttpStatus.BAD_REQUEST,
      );
    }
  }
}
