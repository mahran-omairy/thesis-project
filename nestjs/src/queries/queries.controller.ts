import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpException,
  HttpStatus,
  Post,
  Query,
} from '@nestjs/common';
import { ApiBody, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

import { MetaDto, QueryBodyDto } from './dto/query.body.dto';
import { QueriesService } from './qureies.service';

@ApiTags('Queries')
@Controller('v1/queries')
export class QueriesController {
  constructor(private queriesService: QueriesService) {}

  /**
   * get the components of a configuration resource
   * @param uri string
   * @returns Promise<any>
   */
  @ApiOperation({ summary: 'Load configuration componenets' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Configuration was loaded',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Configuration not found',
  })
  @Get('details')
  async fetchInfo(@Query('uri') uri: string): Promise<any> {
    const config = await this.queriesService.fetchInfo(uri);
    if (!config) {
      throw new HttpException('Cound not load the uri info', 400);
    }
    return config;
  }

  /**
   * run an expansion query
   * @param body QueryBodyDto
   * @returns Promise<any>
   */
  @ApiOperation({ summary: 'Run an expansion query' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Query results',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Could not run the query',
  })
  @Post('/expand')
  @HttpCode(200)
  async expandQuery(@Body() body: QueryBodyDto): Promise<any> {
    return this.queriesService.executeQuery('expand', body);
  }

  /**
   * run a detail query
   * @param body QueryBodyDto
   * @returns Promise<any>
   */
  @ApiOperation({ summary: 'Run a detail query' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Query results',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Could not run the query',
  })
  @Post('/details')
  @HttpCode(200)
  async detailsQuery(@Body() body: QueryBodyDto): Promise<any> {
    return this.queriesService.executeQuery('detail', body);
  }

  /**
   * run a preview query
   * @param body QueryBodyDto
   * @returns Promise<any>
   */
  @ApiOperation({ summary: 'Run a preview query' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Query results',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Could not run the query',
  })
  @Post('/preview')
  @HttpCode(200)
  async previewQuery(@Body() body: QueryBodyDto): Promise<any> {
    return this.queriesService.executeQuery('preview', body);
  }

  /**
   * load a list of meta configuraions
   * @param uri string
   * @returns Promise<any>
   */
  @ApiOperation({ summary: 'Load a list of meta configuraions' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'List results',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Could not run the list',
  })
  @Get('meta-configuration')
  async fetchMeta(@Query('uri') uri: string): Promise<any> {
    const config = await this.queriesService.fetchMeta(uri);
    if (!config) {
      throw new HttpException('Cound not load the uri info', 400);
    }
    return config;
  }

  /**
   * load a list of suggestions components
   * @param meta MetaDto
   * @returns Promise<any>
   */
  @ApiOperation({ summary: 'Load a list of suggestions components' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'List results',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'Could not run the list',
  })
  @Post('suggestions')
  @HttpCode(200)
  async fetchSuggessions(@Body() meta: MetaDto): Promise<any> {
    return this.queriesService.fetchSuggestions(meta.metaList);
  }
}
