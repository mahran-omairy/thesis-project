import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { FilesModule } from './files/files.module';

import { File } from './files/file.entiry';
import { QueriesModule } from './queries/queries.module';

@Module({
  imports: [
    FilesModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.LOCAL_DB_HOST || 'localhost',
      database: process.env.LOCAL_DB_NAME || 'nestjs_app',
      username: process.env.LOCAL_DB_USER || 'app_user',
      password: process.env.LOCAL_DB_PASS || 'app_password',

      port: 3306,

      entities: [File],
      synchronize: true,
    }),
    QueriesModule,
  ],
})
export class AppModule {}
