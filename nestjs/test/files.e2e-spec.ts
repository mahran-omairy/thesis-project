import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';

describe('FilesController (e2e)', () => {
  let app: INestApplication;
  let file = null;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe({ transform: true }));

    await app.init();
  });

  it('/v1/files (POST) 400 no - body', () => {
    return request(app.getHttpServer())
      .post('/v1/files')
      .send({
        name: 'test',
        baseLink: null,
        metaGroup: null,
      })
      .expect(400);
  });

  it('/v1/files (POST) 201', async () => {
    return request(app.getHttpServer())
      .post('/v1/files')
      .send({
        name: 'test',
        body: 'test body',
      })
      .expect(201)
      .then(res => {
        file = res.body;
        expect(file.id).toBeDefined();
      });
  });

  it('/v1/files (GET) list', () => {
    return request(app.getHttpServer())
      .get('/v1/files')
      .expect(200)
      .then(res => {
        expect(res.body.length).toBeGreaterThan(0);
      });
  });

  it('/v1/files/:id (GET) 200', async () => {
    return request(app.getHttpServer())
      .get(`/v1/files/${file.id}`)
      .expect(200);
  });

  it('/v1/files (PUT) 202', async () => {
    await request(app.getHttpServer())
      .put(`/v1/files/${file.id}`)
      .send({
        ...file,
        body: 'test body2',
      })
      .expect(202);
  });

  it('/v1/files/:id (DELETE) 204', async () => {
    return request(app.getHttpServer())
      .delete(`/v1/files/${file.id}`)
      .expect(204);
  });
});
