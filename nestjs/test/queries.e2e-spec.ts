import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';

describe('FilesController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe({ transform: true }));

    await app.init();
  });

  it('/v1/queries/details (GET)', () => {
    return request(app.getHttpServer())
      .get(
        '/v1/queries/details?uri=https://linked.opendata.cz/resource/knowledge-graph-browser/configuration/wikidata/animals',
      )
      .expect(200)
      .then(res => {
        expect(res.body.configs.title.length).toBeGreaterThan(0);
        expect(res.body.viewsets.length).toBeGreaterThan(0);
        expect(res.body.views.length).toBeGreaterThan(0);
        expect(res.body.expansionQueries.length).toBeGreaterThan(0);
        expect(res.body.previewQueries.length).toBeGreaterThan(0);
        expect(res.body.detailQueries.length).toBeGreaterThan(0);
        expect(res.body.datasets.length).toBeGreaterThan(0);
        expect(res.body.stylesheet.length).toBeGreaterThan(0);
        expect(res.body.styles.length).toBeGreaterThan(0);
      });
  });

  it('/v1/queries/expand (POST)', () => {
    return request(app.getHttpServer())
      .post('/v1/queries/expand ')
      .send({
        dataset: {
          sparqlEndpoint: 'https://query.wikidata.org/sparql',
          accept: 'application/sparql-results+json',
        },
        resource: 'http://www.wikidata.org/entity/Q7377',
        sparql:
          'PREFIX wd: <http://www.wikidata.org/entity/> PREFIX wdt: <http://www.wikidata.org/prop/direct/> PREFIX skos: <http://www.w3.org/2004/02/skos/core#> PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX wdab: <https://linked.opendata.cz/resource/vocabulary/knowledge-graph-browser/wikidata/animals/> PREFIX browser: <https://linked.opendata.cz/ontology/knowledge-graph-browser/> CONSTRUCT {   ?parentTaxon a wdab:taxon ;     rdfs:label ?parentTaxonLabel ;     browser:class "taxon", ?parentTaxonClass .        ?node wdab:broader ?parentTaxon .      wdab:broader browser:class "broader" .   } WHERE {    ?node wdt:P171 ?parentTaxon .        OPTIONAL {      ?parentTaxon wdt:P105 ?parentTaxonRank .     ?parentTaxonRank rdfs:label ?parentTaxonRankLabel .     FILTER(lang(?parentTaxonRankLabel) = "en")     BIND(IF(?parentTaxonRank IN (wd:Q7432, wd:Q34740, wd:Q35409, wd:Q36602, wd:Q37517, wd:Q38348, wd:Q36732, wd:Q146481), ?parentTaxonRankLabel, "unsupportedTaxon") AS ?parentTaxonClass)   }      SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }    }',
      })
      .expect(200)
      .then(res => {
        expect(res.body.edges.length).toBeGreaterThan(0);
        expect(res.body.nodes.length).toBeGreaterThan(0);
        expect(res.body.nodes.map(el => el.label).includes('Tetrapoda')).toBe(
          true,
        );
        expect(res.body.types.length).toBeGreaterThan(0);
      });
  });

  it('/v1/queries/preview (POST)', () => {
    return request(app.getHttpServer())
      .post('/v1/queries/preview ')
      .send({
        dataset: {
          sparqlEndpoint: 'https://query.wikidata.org/sparql',
          accept: 'application/sparql-results+json',
        },
        resource: 'http://www.wikidata.org/entity/Q192154',
        sparql:
          'PREFIX wd: <http://www.wikidata.org/entity/> PREFIX wdt: <http://www.wikidata.org/prop/direct/> PREFIX skos: <http://www.w3.org/2004/02/skos/core#> PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX wdab: <https://linked.opendata.cz/resource/vocabulary/knowledge-graph-browser/wikidata/animals/> PREFIX browser: <https://linked.opendata.cz/ontology/knowledge-graph-browser/> CONSTRUCT {   ?node a wdab:taxon ;     rdfs:label ?nodeLabel ;     browser:class "taxon", ?taxonClass .   } WHERE {   ?node wdt:P105 ?taxonRank .      ?taxonRank rdfs:label ?taxonRankLabel .   FILTER(lang(?taxonRankLabel) = "en")      BIND(IF(?taxonRank IN (wd:Q7432, wd:Q34740, wd:Q35409, wd:Q36602, wd:Q37517, wd:Q38348, wd:Q36732, wd:Q146481), ?taxonRankLabel, "unsupportedTaxon") AS ?taxonClass)      SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }     }',
      })
      .expect(200)
      .then(res => {
        expect(res.body.nodes.length).toBeGreaterThan(0);
        expect(res.body.nodes.map(el => el.label).includes('Embryophyte')).toBe(
          true,
        );
        expect(res.body.types.length).toBeGreaterThan(0);
        expect(res.body.types.map(el => el.label).includes('taxon')).toBe(true);
      });
  });

  it('/v1/queries/details (POST)', () => {
    return request(app.getHttpServer())
      .post('/v1/queries/details ')
      .send({
        dataset: {
          sparqlEndpoint: 'https://query.wikidata.org/sparql',
          accept: 'application/sparql-results+json',
        },
        resource: 'http://www.wikidata.org/entity/Q192154',
        sparql:
          'PREFIX wd: <http://www.wikidata.org/entity/> PREFIX wdt: <http://www.wikidata.org/prop/direct/> PREFIX skos: <http://www.w3.org/2004/02/skos/core#> PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX wdab: <https://linked.opendata.cz/resource/vocabulary/knowledge-graph-browser/wikidata/animals/> PREFIX browser: <https://linked.opendata.cz/ontology/knowledge-graph-browser/> CONSTRUCT {   ?node rdfs:label ?nodeLabel ;     wdt:P225 ?p225 ;     wdt:P181 ?p181 ;     wdt:P18 ?p18 .  } WHERE {   ?node wdt:P31 wd:Q16521 .      {     ?node wdt:P225 ?p225 .   } UNION {     ?node wdt:P181 ?p181 .   } UNION {     ?node wdt:P18 ?p18 .   }        SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }    }',
      })
      .expect(200)
      .then(res => {
        expect(res.body.nodes.length).toBeGreaterThan(0);
        expect(
          res.body.nodes
            .map(el => el.iri)
            .includes('http://www.wikidata.org/entity/Q192154'),
        ).toBe(true);
        expect(res.body.types.length).toBeGreaterThan(0);
      });
  });

  it('/v1/queries/meta-configuration (GET)', () => {
    return request(app.getHttpServer())
      .get(
        '/v1/queries/meta-configuration?uri=https://linked.opendata.cz/resource/knowledge-graph-browser/meta-configuration/all-configurations',
      )
      .expect(200)
      .then(res => {
        expect(res.body.length).toBeGreaterThan(0);
        const iris = res.body.map(el => el.iri);
        expect(
          iris.includes(
            'https://linked.opendata.cz/resource/knowledge-graph-browser/meta-configuration/scientists',
          ),
        ).toBe(true);
      });
  });

  it('/v1/queries/suggestions (POST)', () => {
    jest.setTimeout(30000);
    return request(app.getHttpServer())
      .post('/v1/queries/suggestions')
      .send({
        metaList: [
          'https://linked.opendata.cz/resource/knowledge-graph-browser/configuration/people-by-groups',
          'https://linked.opendata.cz/resource/knowledge-graph-browser/configuration/wikidata/scientists',
        ],
      })
      .expect(200)
      .then(res => {
        expect(res.body.length).toBe(2);
      });
  });
});
